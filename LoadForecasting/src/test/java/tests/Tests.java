package tests;

import java.time.DayOfWeek;
import java.util.ArrayList;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import entities.DataSet;
import entities.Profile;
import loadForecastingModels.TemperatureAwareAdjustmentForecaster;
import utilities.Loader;

public class Tests {

	static String PATH_AUSTIN_HOURLY = "src\\main\\resources\\Austin\\Hourly";
	static ArrayList<DataSet> dataSets;

	@BeforeClass
	public static void init() {

		dataSets = Loader.loadDataSets(PATH_AUSTIN_HOURLY);

	}

	@Test
	public void testLoader() {

		DataSet dataSet = dataSets.get(0);
		Assert.assertTrue(dataSet.getIdentifier().equals("1790 (0_24_1)"));
		Assert.assertTrue(dataSet.getProfiles().size() == 731);

		for (Profile profile : dataSet.getProfiles()) {

			Assert.assertTrue(profile.getData().size() == 24);

		}

	}

	@Test
	public void testDataSet() {

		DataSet dataSet = dataSets.get(0);
		Profile profile = dataSet.getProfileByDate(31, 12, 2015);
		DataSet dataSetBefore = dataSet.getDataSetBefore(profile.getDateTime().getDayOfMonth(), profile.getDateTime().getMonth(),
				profile.getDateTime().getYear());
		Assert.assertTrue(dataSet.getProfiles().contains(profile));
		Assert.assertFalse(dataSetBefore.getProfiles().contains(profile));
		dataSet = dataSet.getDataSetBefore(profile.getDateTime().getDayOfMonth(), profile.getDateTime().getMonth(), profile.getDateTime().getYear());
		Assert.assertFalse(dataSet.getProfiles().contains(profile));
		dataSet = dataSets.get(0);
		Profile actual = dataSet.getProfiles().get(100);
		dataSet = dataSet.getDataSetNearestNeighbors(20, actual);
		Assert.assertFalse(dataSet.getProfiles().contains(actual));
		dataSet = dataSets.get(0);
		dataSet = dataSet.getDataSetLast(6);
		Assert.assertTrue(dataSet.getProfiles().size() == 6);

	}
	
	@Test
	public void testTAAF() {
		
		DataSet dataSet = dataSets.get(0);
		dataSet = dataSet.getDataSetEval();
		DataSet helper = dataSet.getDataSetBefore(8, 7, 2016);
		dataSet.getProfiles().removeAll(helper.getProfiles());
		dataSet = dataSets.get(0);
		dataSet = dataSet.getDataSetEval();
		TemperatureAwareAdjustmentForecaster taaf = new TemperatureAwareAdjustmentForecaster(0, 1, 10, 18.33, "Austin", dataSet);
		taaf.calculatePrediction(dataSet.getProfiles().get(0));
		Assert.assertNull(taaf.getPrediction());
		taaf.calculatePrediction(dataSet.getProfiles().get(1));
		Assert.assertNull(taaf.getPrediction());
		taaf.calculatePrediction(dataSet.getProfiles().get(2));
		Assert.assertNull(taaf.getPrediction());
		taaf.calculatePrediction(dataSet.getProfiles().get(3));
		Assert.assertNotNull(taaf.getPrediction());
		taaf.calculatePrediction(dataSet.getProfiles().get(4));
		Assert.assertNull(taaf.getPrediction());
		taaf.calculatePrediction(dataSet.getProfiles().get(5));
		Assert.assertNull(taaf.getPrediction());
		taaf.calculatePrediction(dataSet.getProfiles().get(6));
		Assert.assertNull(taaf.getPrediction());
		taaf.calculatePrediction(dataSet.getProfiles().get(7));
		dataSet = dataSet.getDataSetTraining();
		dataSet = dataSet.getDataSetWithoutHolidays("Austin");
		
		for (Profile profile : dataSet.getProfiles()) {
			
			if (profile.getDateTime().getDayOfWeek().equals(DayOfWeek.SUNDAY)) {
				
				Assert.assertTrue(profile.getDateTime().getDayOfWeek().equals(DayOfWeek.SUNDAY));
				break;
				
			}
			
		}
		
		dataSet = dataSets.get(0);
		taaf = new TemperatureAwareAdjustmentForecaster(0, 1, 10, 18.33, "Austin", dataSet);
		taaf.calculatePrediction(dataSet.getProfileByDate(8, 1, 2015));
		Assert.assertNull(taaf.getPrediction());
		
	}

}