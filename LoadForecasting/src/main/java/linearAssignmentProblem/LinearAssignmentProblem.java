package linearAssignmentProblem;

import java.util.ArrayList;

import entities.Data;
import entities.Profile;

/**
 * 
 * @author http://csclab.murraystate.edu/~bob.pilgrim/445/munkres.html
 *
 */
public class LinearAssignmentProblem {

	private int omega;
	private int p;
	private int type;
	private int pathRowZero;
	private int pathColZero;
	private int pathCount;
	private int resolution;
	private int step;
	private byte[] colCover;
	private byte[] rowCover;
	private int[][] maskMatrix;
	private int[][] path;
	private double[][] costMatrix;
	private Profile baseProfile;
	private Profile previousProfile;

	public LinearAssignmentProblem(int omega, int p, int type, Profile previousProfile, Profile baseProfile) {

		this.omega = omega;
		this.p = p;
		this.type = type;
		this.resolution = baseProfile.getData().size();
		this.step = 0;
		this.colCover = new byte[baseProfile.getData().size()];
		this.rowCover = new byte[baseProfile.getData().size()];
		this.maskMatrix = new int[baseProfile.getData().size()][baseProfile.getData().size()];
		this.path = new int[100][100];
		this.costMatrix = new double[baseProfile.getData().size()][baseProfile.getData().size()];
		this.baseProfile = baseProfile;
		this.previousProfile = previousProfile;

	}
	
	private void augmentPath() {
	
		for (int p = 0; p < pathCount; p++) {
	
			if (maskMatrix[path[p][0]][path[p][1]] == 1) {
	
				maskMatrix[path[p][0]][path[p][1]] = 0;
	
			} else {
	
				maskMatrix[path[p][0]][path[p][1]] = 1;
	
			}
	
		}
	
	}

	private void clearCovers() {
	
		for (int r = 0; r < resolution; r++) {
	
			rowCover[r] = 0;
	
		}
	
		for (int c = 0; c < resolution; c++) {
	
			colCover[c] = 0;
	
		}
	
	}

	private void erasePrimes() {
	
		for (int r = 0; r < resolution; r++) {
	
			for (int c = 0; c < resolution; c++) {
	
				if (maskMatrix[r][c] == 2) {
	
					maskMatrix[r][c] = 0;
	
				}
	
			}
	
		}
	
	}

	private int[] findAZero(int row, int col) {
	
		int r = 0;
		int[] index = new int[2];
		int c;
		boolean done = false;
		row = -1;
		col = -1;
	
		while (!done) {
	
			c = 0;
	
			while (true) {
	
				if (costMatrix[r][c] == 0 && rowCover[r] == 0 && colCover[c] == 0) {
	
					row = r;
					col = c;
					done = true;
	
				}
	
				c = c + 1;
	
				if (c >= resolution || done) {
	
					break;
	
				}
	
			}
	
			r = r + 1;
	
			if (r >= resolution) {
	
				done = true;
	
			}
	
		}
	
		index[0] = row;
		index[1] = col;
	
		return index;
	
	}

	private int findPrimeInRow(int r, int c) {
	
		for (int j = 0; j < resolution; j++) {
	
			if (maskMatrix[r][j] == 2) {
	
				c = j;
	
			}
	
		}
	
		return c;
	
	}

	private double findSmallest(double minValue) {
	
		for (int r = 0; r < resolution; r++) {
	
			for (int c = 0; c < resolution; c++) {
	
				if (rowCover[r] == 0 && colCover[c] == 0) {
	
					if (minValue > costMatrix[r][c]) {
	
						minValue = costMatrix[r][c];
	
					}
	
				}
	
			}
	
		}
	
		return minValue;
	
	}

	private int findStarInCol(int c, int r) {
	
		r = -1;
	
		for (int i = 0; i < resolution; i++) {
	
			if (maskMatrix[i][c] == 1) {
	
				r = i;
	
			}
	
		}
	
		return r;
	
	}

	private int findStarInRow(int row, int col) {
	
		col = -1;
	
		for (int c = 0; c < resolution; c++) {
	
			if (maskMatrix[row][c] == 1) {
	
				col = c;
	
			}
	
		}
	
		return col;
	
	}

	private double[][] getCostMatrix() {

		double error;
		double maxError = 1.0;
		double[][] costMatrix = new double[resolution][resolution];

		if (type == 0) {
			
			for (int col = 0; col < resolution; col++) {

				for (int row = 0; row < resolution; row++) {

					error = getError(previousProfile.getData().get(row).getLoad(), baseProfile.getData().get(col).getLoad());

					if (Math.abs(col - row) <= omega) {

						maxError = maxError + error;
						costMatrix[col][row] = error;

					}

				}

			}
			
		}
		
		if (type == 1) {
			
			for (int col = 0; col < resolution; col++) {

				for (int row = 0; row < resolution; row++) {

					error = getError(previousProfile.getData().get(row).getTemperature(), baseProfile.getData().get(col).getTemperature());

					if (Math.abs(col - row) <= omega) {

						maxError = maxError + error;
						costMatrix[col][row] = error;

					}

				}

			}
			
		}

		for (int col = 0; col < resolution; col++) {

			for (int row = 0; row < resolution; row++) {

				if (Math.abs(col - row) > omega) {

					costMatrix[col][row] = maxError;

				}

			}

		}

		return costMatrix;

	}

	private double getError(double previousProfileLoad, double baseProfileLoad) {

		return Math.pow(Math.abs(previousProfileLoad - baseProfileLoad), p);

	}

	private Profile getPermutedProfile() {

		Profile permuted = new Profile();
		ArrayList<Data> datas = new ArrayList<Data>();

		for (int row = 0; row < resolution; row++) {

			for (int col = 0; col < resolution; col++) {

				if (maskMatrix[row][col] == 1) {

					Data data = new Data();
					data.setHour(previousProfile.getData().get(col).getHour());
					data.setLoad(previousProfile.getData().get(col).getLoad());
					data.setTemperature(previousProfile.getData().get(col).getTemperature());
					datas.add(row, data);

				}

			}

		}

		permuted.setData(datas);
		
		return permuted;

	}

	public Profile getPreviousProfile() {
	
		return previousProfile;
	
	}

	public Profile solve() {
	
		boolean done = false;
	
		while (!done) {
	
			switch (step) {
	
			case 0:
	
				stepZero();
				break;
	
			case 1:
				
				stepOne();
				break;
	
			case 2:
	
				stepTwo();
				break;
	
			case 3:
	
				stepThree();
				break;
	
			case 4:
	
				stepFour();
				break;
	
			case 5:
	
				stepFive();
				break;
	
			case 6:
	
				stepSix();
				break;
	
			case 7:
	
				done = true;
				break;
	
			}
	
		}
		
		return getPermutedProfile();
	
	}

	private boolean starInRow(int row) {
	
		boolean tmp = false;
	
		for (int c = 0; c < resolution; c++) {
	
			if (maskMatrix[row][c] == 1) {
	
				tmp = true;
	
			}
	
		}
	
		return tmp;
	
	}

	private void stepFive() {
	
		boolean done = false;
		int r = -1;
		int c = -1;
	
		pathCount = 1;
		path[pathCount - 1][0] = pathRowZero;
		path[pathCount - 1][1] = pathColZero;
	
		while (!done) {
	
			r = findStarInCol(path[pathCount - 1][1], r);
	
			if (r > -1) {
	
				pathCount = pathCount + 1;
				path[pathCount - 1][0] = r;
				path[pathCount - 1][1] = path[pathCount - 2][1];
	
			} else {
	
				done = true;
	
			}
	
			if (!done) {
	
				c = findPrimeInRow(path[pathCount - 1][0], c);
				pathCount = pathCount + 1;
				path[pathCount - 1][0] = path[pathCount - 2][0];
				path[pathCount - 1][1] = c;
	
			}
	
		}
	
		augmentPath();
		clearCovers();
		erasePrimes();
		step = 3;
	
	}

	private void stepFour() {

		int row = -1;
		int col = -1;
		int[] index = new int[2];
		boolean done = false;

		while (!done) {

			index = findAZero(row, col);
			row = index[0];
			col = index[1];

			if (row == -1) {

				done = true;
				step = 6;

			} else {

				maskMatrix[row][col] = 2;

				if (starInRow(row)) {

					col = findStarInRow(row, col);
					rowCover[row] = 1;
					colCover[col] = 0;

				} else {

					done = true;
					step = 5;
					pathRowZero = row;
					pathColZero = col;

				}

			}

		}

	}

	private void stepOne() {
	
		double minInRow;
	
		for (int row = 0; row < resolution; row++) {
	
			minInRow = costMatrix[row][0];
	
			for (int col = 1; col < resolution; col++) {
	
				if (costMatrix[row][col] < minInRow) {
	
					minInRow = costMatrix[row][col];
	
				}
	
			}
	
			for (int col = 0; col < resolution; col++) {
	
				costMatrix[row][col] = costMatrix[row][col] - minInRow;
	
			}
	
		}

		step = 2;
	
	}

	private void stepSix() {
	
		double minValue = Double.MAX_VALUE;
	
		minValue = findSmallest(minValue);
	
		for (int r = 0; r < resolution; r++) {
	
			for (int c = 0; c < resolution; c++) {
	
				if (rowCover[r] == 1) {
	
					costMatrix[r][c] = costMatrix[r][c] + minValue;
	
				}
	
				if (colCover[c] == 0) {
	
					costMatrix[r][c] = costMatrix[r][c] - minValue;
	
				}
	
			}
	
		}
		
		step = 4;
	
	}

	private void stepThree() {
	
		int colCount = 0;
	
		for (int row = 0; row < resolution; row++) {
	
			for (int col = 0; col < resolution; col++) {
	
				if (maskMatrix[row][col] == 1) {
	
					colCover[col] = 1;
	
				}
	
			}
	
		}
	
		for (int col = 0; col < resolution; col++) {
	
			if (colCover[col] == 1) {
	
				colCount = colCount + 1;
	
			}
	
		}
	
		if (colCount >= resolution) {
	
			step = 7;
	
		} else {
	
			step = 4;
	
		}
	
	}

	private void stepTwo() {
	
		for (int row = 0; row < resolution; row++) {
	
			for (int col = 0; col < resolution; col++) {
	
				if (costMatrix[row][col] == 0 && rowCover[row] == 0 && colCover[col] == 0) {
	
					maskMatrix[row][col] = 1;
					rowCover[row] = 1;
					colCover[col] = 1;
	
				}
	
			}
	
		}
	
		for (int row = 0; row < resolution; row++) {
	
			rowCover[row] = 0;
	
		}
	
		for (int col = 0; col < resolution; col++) {
	
			colCover[col] = 0;
	
		}
		
		step = 3;
	
	}

	private void stepZero() {
	
		costMatrix = getCostMatrix();
		step = 1;
	
	}

}
