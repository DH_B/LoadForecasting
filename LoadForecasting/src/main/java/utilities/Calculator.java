package utilities;

import java.util.ArrayList;
import java.util.Comparator;

import entities.Data;
import entities.DataSet;
import entities.Profile;

public class Calculator {

	public static Profile calculateBaseProfile(Profile actual, DataSet dataSet) {

		Profile baseProfile = new Profile();
		baseProfile.setData(new ArrayList<Data>());

		int resolution = dataSet.getResolution();
		DataSet dataSetBefore = dataSet.getDataSetBefore(actual.getDateTime().getDayOfMonth(), actual.getDateTime().getMonth(),
				actual.getDateTime().getYear());
		ArrayList<Double> load = new ArrayList<Double>();

		for (int indexData = 0; indexData < resolution; indexData++) {

			for (int indexProfile = 0; indexProfile < dataSetBefore.getProfiles().size(); indexProfile++) {

				load.add(dataSetBefore.getProfiles().get(indexProfile).getData().get(indexData).getLoad());

			}

			Data data = new Data();
			data.setHour(actual.getData().get(indexData).getHour());
			data.setMinute(actual.getData().get(indexData).getMinute());
			data.setLoad(getMedian(load));
			data.setTemperature(0.0);
			baseProfile.getData().add(data);
			load.clear();

		}

		baseProfile.calculateMeans();

		return baseProfile;

	}

	private static double getMedian(ArrayList<Double> doubles) {

		doubles.sort(new Comparator<Double>() {

			public int compare(Double o1, Double o2) {

				if (o1 > o2)
					return 1;
				if (o1 < o2)
					return -1;
				return 0;

			}

		});

		double result;

		if (doubles.size() % 2 == 0) {

			double firstDouble = doubles.get((doubles.size() / 2) - 1);
			double secondDouble = doubles.get(doubles.size() / 2);
			result = (firstDouble + secondDouble) / 2.0;

		} else {

			result = doubles.get(doubles.size() / 2);

		}

		return result;

	}

}
