package utilities;

import java.util.ArrayList;

import entities.Data;
import entities.DataSet;
import entities.Profile;
import linearAssignmentProblem.LinearAssignmentProblem;

public class ErrorMetrics {

	private static double calculateAbsoluteScaledError(int indexData, Profile forecasted, Profile actual, DataSet trainingSet) {
	
		double counter = Math.abs(forecasted.getData().get(indexData).getLoad() - actual.getData().get(indexData).getLoad());
		double denominator = 0.0;
	
		for (int indexProfile = 0; indexProfile < trainingSet.getProfiles().size() - 1; indexProfile++) {
	
			denominator = denominator + Math.abs(trainingSet.getProfiles().get(indexProfile).getData().get(indexData).getLoad()
					- trainingSet.getProfiles().get(indexProfile + 1).getData().get(indexData).getLoad());
	
		}
	
		return counter / (denominator / (trainingSet.getProfiles().size() - 1));
	
	}

	private static double calculateAbsoluteScaledError(int indexData, ArrayList<Data> forecasted, ArrayList<Data> actual, DataSet trainingSet) {

		double counter = Math.abs(forecasted.get(indexData).getLoad() - actual.get(indexData).getLoad());
		double denominator = 0.0;

		for (int indexProfile = 0; indexProfile < trainingSet.getProfiles().size() - 1; indexProfile++) {

			denominator = denominator + Math.abs(trainingSet.getProfiles().get(indexProfile).getData().get(indexData).getLoad()
					- trainingSet.getProfiles().get(indexProfile + 1).getData().get(indexData).getLoad());

		}

		return counter / (denominator / (trainingSet.getProfiles().size() - 1));

	}

	public static double calculateMeanAbsoluteError(Profile forecasted, Profile actual) {
	
		double MAE = 0.0;
	
		for (int indexData = 0; indexData < forecasted.getData().size(); indexData++) {
	
			MAE = MAE + Math.abs(forecasted.getData().get(indexData).getLoad() - actual.getData().get(indexData).getLoad());
	
		}
	
		return MAE / forecasted.getData().size();
	
	}

	public static double calculateMeanAbsoluteError(int resolution, ArrayList<Data> forecasted, ArrayList<Data> actual) {

		double MAE = 0.0;

		for (int indexData = 0; indexData < resolution; indexData++) {

			MAE = MAE + Math.abs(forecasted.get(indexData).getLoad() - actual.get(indexData).getLoad());

		}

		return MAE / resolution;

	}

	public static double calculateMeanAbsolutePercentageError(Profile forecasted, Profile actual) {
	
		double MAPE = 0.0;
	
		for (int indexData = 0; indexData < forecasted.getData().size(); indexData++) {
	
			MAPE = MAPE + Math.abs(
					(forecasted.getData().get(indexData).getLoad() - actual.getData().get(indexData).getLoad()) / actual.getData().get(indexData).getLoad());
	
		}
	
		return 100 * (MAPE / forecasted.getData().size());
	
	}

	public static double calculateMeanAbsolutePercentageError(int resolution, ArrayList<Data> forecasted, ArrayList<Data> actual) {

		double MAPE = 0.0;

		for (int indexData = 0; indexData < resolution; indexData++) {

			MAPE = MAPE + Math.abs((forecasted.get(indexData).getLoad() - actual.get(indexData).getLoad()) / actual.get(indexData).getLoad());

		}

		return 100 * (MAPE / resolution);

	}

	public static double calculateMeanAbsoluteScaledError(Profile forecasted, Profile actual, DataSet trainingSet) {
	
		double MASE = 0.0;
	
		for (int indexData = 0; indexData < forecasted.getData().size(); indexData++) {
	
			MASE = MASE + calculateAbsoluteScaledError(indexData, forecasted, actual, trainingSet);
	
		}
	
		return MASE / forecasted.getData().size();
	
	}

	public static double calculateMeanAbsoluteScaledError(int resolution, ArrayList<Data> forecasted, ArrayList<Data> actual, DataSet trainingSet) {

		double MASE = 0.0;

		for (int indexData = 0; indexData < resolution; indexData++) {

			MASE = MASE + calculateAbsoluteScaledError(indexData, forecasted, actual, trainingSet);

		}

		return MASE / resolution;

	}

	public static double calculateNormalizedRootMeanSquareError(Profile forecasted, Profile actual) {
	
		double counter = 0.0;
		double denominator = 0.0;
	
		for (int indexData = 0; indexData < forecasted.getData().size(); indexData++) {
	
			counter = counter + Math.pow((forecasted.getData().get(indexData).getLoad() - actual.getData().get(indexData).getLoad()), 2);
			denominator = denominator + Math.pow(actual.getData().get(indexData).getLoad(), 2);
	
		}
	
		return Math.pow((counter / denominator), 0.5);
	
	}
	
	public static double calculateNormalizedRootMeanSquareErrorByTemperature(Profile forecasted, Profile actual) {
		
		double counter = 0.0;
		double denominator = 0.0;
	
		for (int indexData = 0; indexData < forecasted.getData().size(); indexData++) {
	
			counter = counter + Math.pow((forecasted.getData().get(indexData).getTemperature() - actual.getData().get(indexData).getTemperature()), 2);
			denominator = denominator + Math.pow(actual.getData().get(indexData).getTemperature(), 2);
	
		}
	
		return Math.pow((counter / denominator), 0.5);
	
	}

	public static double calculateNormalizedRootMeanSquareErrorByMeanOfActual(Profile forecasted, Profile actual) {
	
		double mean = 0.0;
	
		for (int indexData = 0; indexData < actual.getData().size(); indexData++) {
	
			mean = mean + actual.getData().get(indexData).getLoad();
	
		}
	
		return calculateRootMeanSquareError(forecasted, actual) / (mean / actual.getData().size());
	
	}

	public static double calculateNormalizedRootMeanSquareError(int resolution, ArrayList<Data> forecasted, ArrayList<Data> actual) {

		double counter = 0.0;
		double denominator = 0.0;

		for (int indexData = 0; indexData < resolution; indexData++) {

			counter = counter + Math.pow((forecasted.get(indexData).getLoad() - actual.get(indexData).getLoad()), 2);
			denominator = denominator + Math.pow(actual.get(indexData).getLoad(), 2);

		}

		return Math.pow((counter / denominator), 0.5);

	}

	public static double calculatePNorm(int omega, int p, Profile forecasted, Profile actual) {
	
		LinearAssignmentProblem linAssProblem = new LinearAssignmentProblem(omega, p, 0, forecasted, actual);
		Profile permuted = linAssProblem.solve();
		return calculatePRootMeanSquareError(p, permuted, actual);
	
	}

	public static double calculatePRootMeanSquareError(int p, Profile forecasted, Profile actual) {
	
		double RMSE = 0.0;
	
		for (int indexData = 0; indexData < forecasted.getData().size(); indexData++) {
	
			RMSE = RMSE + Math.pow(Math.abs((forecasted.getData().get(indexData).getLoad() - actual.getData().get(indexData).getLoad())), p);
	
		}
	
		return Math.pow((RMSE / forecasted.getData().size()), (1.0 / p));
	
	}

	public static double calculatePRootMeanSquareErrorByTemperature(int p, Profile permuted, Profile actual) {
		
		double RMSE = 0.0;
	
		for (int indexData = 0; indexData < permuted.getData().size(); indexData++) {
	
			RMSE = RMSE + Math.pow(Math.abs((permuted.getData().get(indexData).getTemperature() - actual.getData().get(indexData).getTemperature())), p);
	
		}
	
		return Math.pow((RMSE / permuted.getData().size()), (1.0 / p));
	
	}

	public static double calculateRootMeanSquareError(int resolution, ArrayList<Data> forecasted, ArrayList<Data> actual) {

		double RMSE = 0.0;

		for (int indexData = 0; indexData < resolution; indexData++) {

			RMSE = RMSE + Math.pow((forecasted.get(indexData).getLoad() - actual.get(indexData).getLoad()), 2);

		}

		return Math.pow((RMSE / resolution), 0.5);

	}

	public static double calculateSymmetricMeanAbsolutePercentageError(Profile forecasted, Profile actual) {
	
		double SMAPE = 0.0;
	
		for (int indexData = 0; indexData < forecasted.getData().size(); indexData++) {
	
			SMAPE = SMAPE + (Math.abs(forecasted.getData().get(indexData).getLoad() - actual.getData().get(indexData).getLoad())
					/ (Math.abs(forecasted.getData().get(indexData).getLoad()) + Math.abs(actual.getData().get(indexData).getLoad())));
	
		}
	
		return 100 * (SMAPE / forecasted.getData().size());
	
	}

	public static double calculateSymmetricMeanAbsolutePercentageError(int resolution, ArrayList<Data> forecasted, ArrayList<Data> actual) {

		double SMAPE = 0.0;

		for (int indexData = 0; indexData < resolution; indexData++) {

			SMAPE = SMAPE + (Math.abs(forecasted.get(indexData).getLoad() - actual.get(indexData).getLoad())
					/ (Math.abs(forecasted.get(indexData).getLoad()) + Math.abs(actual.get(indexData).getLoad())));

		}

		return 100 * (SMAPE / resolution);

	}

	public static double calculateRootMeanSquareError(Profile forecasted, Profile actual) {

		double RMSE = 0.0;

		for (int indexData = 0; indexData < forecasted.getData().size(); indexData++) {

			RMSE = RMSE + Math.pow(Math.abs((forecasted.getData().get(indexData).getLoad() - actual.getData().get(indexData).getLoad())), 2);

		}

		return Math.pow((RMSE / forecasted.getData().size()), 0.5);

	}

}