package utilities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import entities.Data;
import entities.DataSet;
import entities.DateTime;
import entities.Profile;

public class Loader {

	private static ArrayList<DataSet> getDataSets(String directory, String path) {

		ArrayList<DataSet> dataSets = new ArrayList<DataSet>();

		String[] identifiers = path.split("\\.")[0].split("_");
		int resolution = Integer.valueOf(identifiers[0]);
		int aggregation = Integer.valueOf(identifiers[1]);

		try {

			BufferedReader bufferedReader = new BufferedReader(new FileReader(directory.concat(path)));

			String date;
			String line;
			String time;
			String[] dateTime;
			String[] fragments;
			String[] timeOffset;
			int dayOfMonth;
			int month;
			int year;
			int dayOfWeek;
			int minute;
			int hour;
			int numHouseholds;
			double load;
			double temperature;
			Calendar calendar = new GregorianCalendar();

			line = bufferedReader.readLine();
			fragments = line.split(";");
			numHouseholds = fragments.length - 2;

			for (int indexHousehold = 0; indexHousehold < numHouseholds; indexHousehold++) {

				DataSet dataSet = new DataSet(resolution, new ArrayList<Profile>());
				dataSet.setIdentifier(fragments[indexHousehold + 2]
						.concat(" (" + String.valueOf(indexHousehold) + "_" + resolution + "_" + aggregation + ")"));
				dataSets.add(dataSet);

			}

			while ((line = bufferedReader.readLine()) != null) {

				fragments = line.split(";");

				dateTime = fragments[0].split("T");
				date = dateTime[0];

				dayOfMonth = Integer.valueOf(date.split("-")[2]);
				month = Integer.valueOf(date.split("-")[1]);
				year = Integer.valueOf(date.split("-")[0]);

				timeOffset = dateTime[1].split("-");
				time = timeOffset[0];

				minute = Integer.valueOf(time.split(":")[1]);
				hour = Integer.valueOf(time.split(":")[0]);

				calendar.set(year, month - 1, dayOfMonth - 1);

				dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

				temperature = Double.valueOf(fragments[1]);

				DateTime dt = new DateTime();
				dt.setDayOfMonth(dayOfMonth);
				dt.setMonth(month);
				dt.setYear(year);
				dt.setDayOfWeek(DayOfWeek.of(dayOfWeek));

				for (int indexHousehold = 0; indexHousehold < numHouseholds; indexHousehold++) {

					load = Double.valueOf(fragments[indexHousehold + 2]) / aggregation;

					if (dataSets.get(indexHousehold).getProfiles().isEmpty()) {

						Profile profile = new Profile();
						profile.setData(new ArrayList<Data>());
						profile.setDateTime(dt);

						dataSets.get(indexHousehold).getProfiles().add(profile);

					}

					if (dataSets.get(indexHousehold).getProfiles()
							.get(dataSets.get(indexHousehold).getProfiles().size() - 1).getDateTime()
							.getDayOfMonth() != dayOfMonth) {

						dataSets.get(indexHousehold).getProfiles()
								.get(dataSets.get(indexHousehold).getProfiles().size() - 1).calculateMeans();

						Profile profile = new Profile();
						profile.setData(new ArrayList<Data>());
						profile.setDateTime(dt);

						dataSets.get(indexHousehold).getProfiles().add(profile);

					}

					Data data = new Data();
					data.setMinute(minute);
					data.setHour(hour);
					data.setLoad(load);
					data.setTemperature(temperature);

					dataSets.get(indexHousehold).getProfiles()
							.get(dataSets.get(indexHousehold).getProfiles().size() - 1).getData().add(data);

				}

			}

			for (int indexHousehold = 0; indexHousehold < numHouseholds; indexHousehold++) {

				dataSets.get(indexHousehold).handleTimeChange();
				dataSets.get(indexHousehold).getProfiles().get(dataSets.get(indexHousehold).getProfiles().size() - 1)
						.calculateMeans();

			}

			bufferedReader.close();

		} catch (FileNotFoundException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		}

		return dataSets;

	}

	public static ArrayList<DataSet> loadDataSets(String directory) {

		ArrayList<DataSet> dataSets = new ArrayList<DataSet>();
		String[] fileNames = new File(directory).list(new FilenameFilter() {

			public boolean accept(File dir, String name) {

				if (name.endsWith(".csv") || name.endsWith(".CSV")) {

					return true;

				} else {

					return false;

				}

			}

		});

		for (int indexFile = 0; indexFile < fileNames.length; indexFile++) {

			dataSets.addAll(getDataSets(directory + "\\", fileNames[indexFile]));

		}

		return dataSets;

	}

}