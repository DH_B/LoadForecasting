package evaluation;

import java.util.ArrayList;
import java.util.concurrent.Callable;

import entities.DataSet;
import entities.Profile;
import loadForecastingModels.AveragedAdjustmentForecaster;

public class EvaluatorAAF implements Callable<ResultAAF> {

	private int omega;
	private int p;
	private DataSet dataSetEval;
	
	public EvaluatorAAF(int omega, int p, DataSet dataSetEval) {
		
		this.omega = omega;
		this.p = p;
		this.dataSetEval = dataSetEval;
		
	}

	@Override
	public ResultAAF call() throws Exception {
		
		ArrayList<Profile> profilesActual = new ArrayList<Profile>();
		ArrayList<Profile> profilesPredicted = new ArrayList<Profile>();
		AveragedAdjustmentForecaster AAF = new AveragedAdjustmentForecaster(omega, p, dataSetEval);
		
		for (Profile profile : dataSetEval.getProfiles()) {
			
			if (profile.getDateTime().isBefore(8, 7, 2016)) {

				if (!profile.getDateTime().isSameDate(4, 7, 2016)) {

					continue;

				}

			}

			AAF.calculatePrediction(profile);

			if (AAF.getPrediction() != null) {
				
				profilesActual.add(profile);
				profilesPredicted.add(AAF.getPrediction());

			}

		}

		
		return new ResultAAF(omega, p, profilesPredicted, profilesActual, dataSetEval);
	
	}
	
	public DataSet getDataSetEval() {
		
		return dataSetEval;
		
	}

	public int getOmega() {
	
		return omega;
	
	}

	public int getP() {
	
		return p;
	
	}

	@Override
	public String toString() {
	
		return "EvaluatorAAF [omega=" + omega + ", p=" + p + "]";
	
	}

}
