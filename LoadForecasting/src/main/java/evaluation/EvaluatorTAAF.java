package evaluation;

import java.util.ArrayList;
import java.util.concurrent.Callable;

import entities.DataSet;
import entities.Profile;
import loadForecastingModels.TemperatureAwareAdjustmentForecaster;

public class EvaluatorTAAF implements Callable<ResultTAAF> {

	private int omega;
	private int p;
	private int k;
	private double baseTemperature;
	private DataSet dataSetTraining;
	private DataSet dataSetEval;
	private String location;

	public EvaluatorTAAF(int omega, int p, int k, double baseTemperature, DataSet dataSetTraining, DataSet dataSetEval, String location) {
		
		this.omega = omega;
		this.p = p;
		this.k = k;
		this.baseTemperature = baseTemperature;
		this.dataSetTraining = dataSetTraining;
		this.dataSetEval = dataSetEval;
		this.location = location;

	}

	@Override
	public ResultTAAF call() throws Exception {

		ArrayList<Profile> profilesActual = new ArrayList<Profile>();
		ArrayList<Profile> profilesPredicted = new ArrayList<Profile>();
		TemperatureAwareAdjustmentForecaster TAAF = new TemperatureAwareAdjustmentForecaster(omega, p, k, baseTemperature, location, dataSetEval);

		for (Profile profile : dataSetEval.getProfiles()) {

			TAAF.calculatePrediction(profile);

			if (TAAF.getPrediction() != null) {
				
				profilesActual.add(profile);
				profilesPredicted.add(TAAF.getPrediction());

			}

		}

		return new ResultTAAF(omega, p, k, profilesPredicted, profilesActual, dataSetTraining);

	}

}