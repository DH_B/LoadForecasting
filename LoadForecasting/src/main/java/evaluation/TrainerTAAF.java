package evaluation;

import java.util.ArrayList;
import java.util.concurrent.Callable;

import entities.DataSet;
import entities.Profile;
import loadForecastingModels.TemperatureAwareAdjustmentForecaster;
import utilities.ErrorMetrics;

public class TrainerTAAF implements Callable<ResultTAAF> {

	private int omega;
	private int p;
	private double baseTemperature;
	private String location;
	private DataSet dataSetTraining;

	public TrainerTAAF(int omega, int p, double baseTemperature, DataSet dataSetTraining, String location) {

		this.omega = omega;
		this.p = p;
		this.baseTemperature = baseTemperature;
		this.location = location;
		this.dataSetTraining = dataSetTraining;
		
	}

	@Override
	public ResultTAAF call() throws Exception {
		
		int[] ks = new int[] { 1, 2, 4, 8, 16 };

		int kMin = -1;
		int omegaMin = -1;
		double error;
		double errorMin = Double.MAX_VALUE;
		ArrayList<Profile> profilesActualMin = new ArrayList<Profile>();
		ArrayList<Profile> profilesForecastedMin = new ArrayList<Profile>();
		
		for (int k : ks) {

			error = 0.0;
			ArrayList<Profile> profilesActual = new ArrayList<Profile>();
			ArrayList<Profile> profilesForecasted = new ArrayList<Profile>();
			TemperatureAwareAdjustmentForecaster TAAF = new TemperatureAwareAdjustmentForecaster(omega, p, k, baseTemperature, location, dataSetTraining);

			for (Profile profile : dataSetTraining.getProfiles()) {
				
				TAAF.calculatePrediction(profile);

				if (TAAF.getPrediction() != null) {

					error = error + ErrorMetrics.calculatePNorm(omega, p, TAAF.getPrediction(), profile);
					profilesActual.add(profile);
					profilesForecasted.add(TAAF.getPrediction());

				}

			}

			if (error < errorMin) {

				kMin = k;
				omegaMin = omega;
				errorMin = error;
				profilesActualMin = profilesActual;
				profilesForecastedMin = profilesForecasted;

			}

		}

		return new ResultTAAF(omegaMin, p, kMin, profilesForecastedMin, profilesActualMin, dataSetTraining);
	
	}

}