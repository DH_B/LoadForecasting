package evaluation;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import entities.DataSet;
import utilities.Loader;

public class EvaluationTAAF {

	static String PATH_AUSTIN_HOURLY = "src\\main\\resources\\Austin\\Hourly";
	static String PATH_AUSTIN_QUARTERLY = "src\\main\\resources\\Austin\\Quarterly";
	static String PATH_BOULDER_HOURLY = "src\\main\\resources\\Boulder\\Hourly";
	static String PATH_BOULDER_QUARTERLY = "src\\main\\resources\\Boulder\\Quarterly";

	static String path;

	public static void main(String[] args) {

		evaluate(PATH_BOULDER_HOURLY);

	}

	private static ArrayList<String> concat(String identifier, ArrayList<String> result, ArrayList<String> concats) {
		
		try {
			
			concats.set(0, concats.get(0) + ";" + identifier);
			
			for (int indexLine = 0; indexLine < result.size(); indexLine++) {

				concats.set(indexLine + 1, concats.get(indexLine + 1) + ";" + result.get(indexLine));

			}

		} catch (IndexOutOfBoundsException e) {
			
			concats.add(";" + identifier);
			
			for (int indexLine = 0; indexLine < result.size() / 10; indexLine++) {

				concats.add("OMEGA" + ";" + result.get(indexLine * 10 + 0));
				concats.add("P" + ";" + result.get(indexLine * 10 + 1));
				concats.add("K" + ";" + result.get(indexLine * 10 + 2));
				concats.add("PNORMAVE" + ";" + result.get(indexLine * 10 + 3));
				concats.add("MAEAVE" + ";" + result.get(indexLine * 10 + 4));
				concats.add("MASEAVE" + ";" + result.get(indexLine * 10 + 5));
				concats.add("NRMSEAVE" + ";" + result.get(indexLine * 10 + 6));
				concats.add("NRMSEMAVE" + ";" + result.get(indexLine * 10 + 7));
				concats.add("RMSEAVE" + ";" + result.get(indexLine * 10 + 8));
				concats.add("SMAPEAVE" + ";" + result.get(indexLine * 10 + 9));

			}

		}
		
		return concats;

	}

	private static void evaluate(String pathToDir) {
	
		path = pathToDir;
		String location = "";
		double startTime;
	
		if (path.equals(PATH_AUSTIN_HOURLY) || path.equals(PATH_AUSTIN_QUARTERLY)) {
	
			location = "Austin";
	
		}
	
		if (path.equals(PATH_BOULDER_HOURLY) || path.equals(PATH_BOULDER_QUARTERLY)) {
	
			location = "Boulder";
	
		}
	
		ArrayList<DataSet> dataSets = Loader.loadDataSets(path);
		ArrayList<ArrayList<String>> result = new ArrayList<ArrayList<String>>();
		ArrayList<String> concats = new ArrayList<String>();
	
		for (int selector = 0; selector < dataSets.size(); selector++) {
	
			System.out.println(dataSets.get(selector).getIdentifier());
			startTime = System.currentTimeMillis();
			result = toString(dataSets.get(selector).getIdentifier(), evaluate(location, dataSets.get(selector)));
			print("\\Forecasts\\TAAF\\TAAF_Forecasts_" + dataSets.get(selector).getIdentifier(), result.get(1));
			concats = concat(dataSets.get(selector).getIdentifier(), result.get(0), concats);
			System.out.println("DURATION " + ((System.currentTimeMillis() - startTime) / 1000.0) + "sec");
	
		}
		
		print("\\Forecasts\\TAAF\\TAAF_ErrorMetrics", concats);
	
	}

	private static ArrayList<ResultTAAF> evaluate(String location, DataSet dataSet) {

		double baseTemperature = 18.33;
		int[] ps = new int[] { 1, 2, 4 };
		int[] omegas = new int[] { 0, 1, 2, 4, 8, 12 };

		DataSet dataSetTraining = dataSet.getDataSetTraining();
		DataSet dataSetEval = dataSet.getDataSetEval();

		ExecutorService executor = Executors.newFixedThreadPool(18);
		ArrayList<Future<ResultTAAF>> resultsTrainingFuture = new ArrayList<Future<ResultTAAF>>();

		System.out.println("START TRAINING");

		for (int omega : omegas) {

			for (int p : ps) {

				synchronized (resultsTrainingFuture) {

					resultsTrainingFuture.add(
							executor.submit(new TrainerTAAF(omega, p, baseTemperature, dataSetTraining, location)));

				}

			}

		}

		executor.shutdown();

		try {

			executor.awaitTermination(3, TimeUnit.HOURS);

		} catch (InterruptedException e) {

			e.printStackTrace();

		}

		executor = Executors.newFixedThreadPool(18);
		ArrayList<Future<ResultTAAF>> resultsTestingFuture = new ArrayList<Future<ResultTAAF>>();

		System.out.println("START TESTING");

		for (Future<ResultTAAF> resultFuture : resultsTrainingFuture) {

			synchronized (resultsTestingFuture) {

				try {

					resultsTestingFuture.add(executor.submit(new EvaluatorTAAF(resultFuture.get().getOmega(),
							resultFuture.get().getP(), resultFuture.get().getK(), baseTemperature,
							dataSetTraining.getCopy(), dataSetEval.getCopy(), location)));

				} catch (InterruptedException | ExecutionException e) {

					e.printStackTrace();

				}

			}

		}

		executor.shutdown();

		try {

			executor.awaitTermination(3, TimeUnit.HOURS);

		} catch (InterruptedException e) {

			e.printStackTrace();

		}

		ArrayList<ResultTAAF> resultsTesting = new ArrayList<ResultTAAF>();

		for (int indexResult = 0; indexResult < (ps.length * omegas.length); indexResult++) {

			try {

				resultsTesting.add(resultsTestingFuture.get(indexResult).get());

			} catch (InterruptedException | ExecutionException e) {

				e.printStackTrace();

			}

		}

		resultsTesting.sort(new Comparator<ResultTAAF>() {

			@Override
			public int compare(ResultTAAF o1, ResultTAAF o2) {

				if (o1.getOmega() > o2.getOmega()) {

					return 1;

				}

				if (o1.getOmega() < o2.getOmega()) {

					return -1;

				}

				if (o1.getOmega() == o2.getOmega()) {

					if (o1.getP() > o2.getP()) {

						return 1;

					}

					if (o1.getP() < o2.getP()) {

						return -1;

					}

				}

				return 0;

			}

		});

		return resultsTesting;

	}

	private static void print(String target, ArrayList<String> lines) {

		BufferedWriter bufferedWriter;

		try {

			bufferedWriter = new BufferedWriter(new FileWriter(path + target + ".csv"));

			for (int indexLine = 0; indexLine < lines.size(); indexLine++) {

				bufferedWriter.write(lines.get(indexLine) + "\n");

			}

			bufferedWriter.close();

		} catch (IOException e) {

			e.printStackTrace();

		}

	}

	private static ArrayList<ArrayList<String>> toString(String identifier, ArrayList<ResultTAAF> resultsTAAF) {

		ArrayList<ArrayList<String>> result = new ArrayList<ArrayList<String>>();

		ArrayList<String> errorMetrics = new ArrayList<String>();
		ArrayList<String> forecasts = new ArrayList<String>();
		
		for (int indexResult = 0; indexResult < resultsTAAF.size(); indexResult++) {

			errorMetrics.add(String.valueOf(resultsTAAF.get(indexResult).getOmega()));
			errorMetrics.add(String.valueOf(resultsTAAF.get(indexResult).getP()));
			errorMetrics.add(String.valueOf(resultsTAAF.get(indexResult).getK()));
			errorMetrics.add(String.valueOf(resultsTAAF.get(indexResult).getpNormAve()));
			errorMetrics.add(String.valueOf(resultsTAAF.get(indexResult).getMAEAve()));
			errorMetrics.add(String.valueOf(resultsTAAF.get(indexResult).getMASEAve()));
			errorMetrics.add(String.valueOf(resultsTAAF.get(indexResult).getNRMSEAve()));
			errorMetrics.add(String.valueOf(resultsTAAF.get(indexResult).getNRMSEM()));
			errorMetrics.add(String.valueOf(resultsTAAF.get(indexResult).getRMSEAve()));
			errorMetrics.add(String.valueOf(resultsTAAF.get(indexResult).getsMAPEAve()));

			try {
				
				forecasts.set(0, forecasts.get(0) + ";" + resultsTAAF.get(indexResult).getOmega() + " " + resultsTAAF.get(indexResult).getP() + " " + resultsTAAF.get(indexResult).getK());
				
			} catch (IndexOutOfBoundsException e) {
				
				forecasts.add("Date;" + resultsTAAF.get(indexResult).getOmega() + " " + resultsTAAF.get(indexResult).getP() + " " + resultsTAAF.get(indexResult).getK());
				
			}
			
			for (int indexProfile = 0; indexProfile < resultsTAAF.get(indexResult).getForecasts()
					.size(); indexProfile++) {

				for (int indexData = 0; indexData < resultsTAAF.get(indexResult).getForecasts().get(indexProfile)
						.getData().size(); indexData++) {

					try {

						forecasts.set(1 + indexData + 24 * indexProfile,
								forecasts.get(1 + indexData + 24 * indexProfile) + ";"
										+ String.valueOf(resultsTAAF.get(indexResult).getForecasts().get(indexProfile)
												.getData().get(indexData).getLoad()));

					} catch (IndexOutOfBoundsException e) {

						forecasts.add(resultsTAAF.get(indexResult).getForecasts().get(indexProfile).getDateTime()
								.getDayOfMonth() + "-"
								+ resultsTAAF.get(indexResult).getForecasts().get(indexProfile).getDateTime().getMonth()
								+ "-"
								+ resultsTAAF.get(indexResult).getForecasts().get(indexProfile).getDateTime().getYear()
								+ "T"
								+ resultsTAAF.get(indexResult).getForecasts().get(indexProfile).getData().get(indexData)
										.getHour()
								+ ":"
								+ resultsTAAF.get(indexResult).getForecasts().get(indexProfile).getData().get(indexData)
										.getMinute()
								+ ";" + String.valueOf(resultsTAAF.get(indexResult).getForecasts().get(indexProfile)
										.getData().get(indexData).getLoad()));

					}

				}

			}

		}
		
		result.add(errorMetrics);
		result.add(forecasts);

		return result;

	}

}