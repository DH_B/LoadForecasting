package evaluation;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import entities.Data;
import entities.DataSet;
import entities.Profile;
import loadForecastingModels.MovingAverage;
import utilities.Loader;

public class EvaluationMA {

	static String PATH_AUSTIN_HOURLY = "src\\main\\resources\\Austin\\Hourly";
	static String PATH_AUSTIN_QUARTERLY = "src\\main\\resources\\Austin\\Quarterly";
	static String PATH_BOULDER_HOURLY = "src\\main\\resources\\Boulder\\Hourly";
	static String PATH_BOULDER_QUARTERLY = "src\\main\\resources\\Boulder\\Quarterly";

	static String path;

	public static void main(String[] args) {

		evaluate(PATH_BOULDER_HOURLY);

	}

	private static ArrayList<String> concat(ArrayList<String> result, ArrayList<String> concatenations) {
	
		try {
	
			for (int indexLine = 0; indexLine < result.size(); indexLine++) {
	
				concatenations.set(indexLine, concatenations.get(indexLine) + ";" + result.get(indexLine));
	
			}
	
		} catch (IndexOutOfBoundsException e) {
	
			int counter = 1;
			int[] ps = new int[] { 1, 2, 4 };
			int[] omegas = new int[] { 0, 1, 2, 4, 8, 12 };
	
			concatenations.add(";" + result.get(0));
	
			for (int indexOmega = 0; indexOmega < omegas.length; indexOmega++) {
	
				concatenations.add("OMEGA; " + result.get(counter++));
	
				for (int indexP = 0; indexP < ps.length; indexP++) {
	
					concatenations.add("P;" + result.get(counter++));
					concatenations.add("PNORMAVE;" + result.get(counter++));
	
				}
	
			}
	
			concatenations.add("MAEAVE" + ";" + result.get(counter++));
			concatenations.add("NRMSEAVE" + ";" + result.get(counter++));
			concatenations.add("NRMSEMAVE" + ";" + result.get(counter++));
			concatenations.add("RMSEAVE" + ";" + result.get(counter++));
			concatenations.add("SMAPEAVE" + ";" + result.get(counter++));
	
		}
	
		return concatenations;
	
	}

	private static void evaluate(String pathToDir) {

		path = pathToDir;
		String location = "";
		double startTime;

		if (path.equals(PATH_AUSTIN_HOURLY) || path.equals(PATH_AUSTIN_QUARTERLY)) {

			location = "Austin";

		}

		if (path.equals(PATH_BOULDER_HOURLY) || path.equals(PATH_BOULDER_QUARTERLY)) {

			location = "Boulder";

		}

		ArrayList<String> concatenations = new ArrayList<String>();
		ArrayList<DataSet> dataSets = Loader.loadDataSets(path);

		for (int selector = 0; selector < dataSets.size(); selector++) {

			System.out.println(dataSets.get(selector).getIdentifier());
			startTime = System.currentTimeMillis();
			concatenations = concat(
					toString(dataSets.get(selector).getIdentifier(), evaluate(location, dataSets.get(selector))),
					concatenations);
			System.out.println("DURATION " + ((System.currentTimeMillis() - startTime) / 1000.0) + "sec");

		}

		print("\\Forecasts\\MA\\MA_ErrorMetrics.csv", concatenations);

	}

	private static ResultMA evaluate(String location, DataSet dataSet) {
	
		return performEvaluation(dataSet);
	
	}

	private static ResultMA performEvaluation(DataSet dataSet) {
	
		ArrayList<Profile> profilesForecasted = new ArrayList<Profile>();
		ArrayList<Profile> profilesActual = new ArrayList<Profile>();
	
		for (int indexProfile = 0; indexProfile < dataSet.getProfiles().size(); indexProfile++) {
	
			if (dataSet.getProfiles().get(indexProfile).getDateTime().isBefore(8, 7, 2016)) {
	
				if (!dataSet.getProfiles().get(indexProfile).getDateTime().isSameDate(4, 7, 2016)) {
	
					continue;
	
				}
	
			}
	
			if (dataSet.getProfiles().get(indexProfile).getDateTime().isBefore(1, 7, 2016)) {
	
				continue;
	
			}
	
			ArrayList<Data> datas = new ArrayList<Data>();
			Profile predicted = new Profile();
	
			for (int indexStep = 0; indexStep < dataSet.getResolution(); indexStep++) {
	
				MovingAverage MA = new MovingAverage(dataSet);
				MA.calculatePrediction(indexStep, dataSet.getProfiles().get(indexProfile));
				datas.add(MA.getPrediction());
	
			}
	
			if (!datas.contains(null)) {
	
				predicted.setData(datas);
				predicted.setDateTime(dataSet.getProfiles().get(indexProfile).getDateTime());
				predicted.calculateMeans();
				profilesForecasted.add(predicted);
				profilesActual.add(dataSet.getProfiles().get(indexProfile));
	
			}
	
		}
	
		print("\\Forecasts\\MA\\MA_Forecasts_"+ dataSet.getIdentifier() + ".csv", toString(profilesForecasted, profilesActual));
	
		return new ResultMA(profilesForecasted, profilesActual, dataSet);
	
	}

	private static void print(String target, ArrayList<String> lines) {

		BufferedWriter bufferedWriter;

		try {

			bufferedWriter = new BufferedWriter(new FileWriter(path + target));

			for (int indexLine = 0; indexLine < lines.size(); indexLine++) {

				bufferedWriter.write(lines.get(indexLine) + "\n");

			}

			bufferedWriter.close();

		} catch (IOException e) {

			e.printStackTrace();

		}

	}

	private static ArrayList<String> toString(String identifier, ResultMA resultMA) {

		int[] ps = new int[] { 1, 2, 4 };
		int[] omegas = new int[] { 0, 1, 2, 4, 8, 12 };
		ArrayList<String> results = new ArrayList<String>();

		results.add(identifier);

		for (int omega : omegas) {

			results.add(String.valueOf(omega));

			for (int p : ps) {

				results.add(String.valueOf(p));
				results.add(String.valueOf(resultMA.calculatePNormAve(omega, p)));

			}

		}

		results.add(String.valueOf(resultMA.getMAEAve()));
		results.add(String.valueOf(resultMA.getNRMSEAve()));
		results.add(String.valueOf(resultMA.getNRMSEMAve()));
		results.add(String.valueOf(resultMA.getRMSEAve()));
		results.add(String.valueOf(resultMA.getsMAPEAve()));

		return results;

	}

	private static ArrayList<String> toString(ArrayList<Profile> profilesForecasted,
			ArrayList<Profile> profilesActual) {
	
		ArrayList<String> strings = new ArrayList<String>();
		
		strings.add("Date;Temperature;Forecasted;Actual");
	
		for (int indexProfile = 0; indexProfile < profilesForecasted.size(); indexProfile++) {
	
			for (int indexData = 0; indexData < profilesForecasted.get(indexProfile).getData().size(); indexData++) {
	
				strings.add(profilesForecasted.get(indexProfile).getDateTime().getDayOfMonth() + "-"
						+ profilesForecasted.get(indexProfile).getDateTime().getMonth() + "-"
						+ profilesForecasted.get(indexProfile).getDateTime().getYear() + "T"
						+ profilesForecasted.get(indexProfile).getData().get(indexData).getHour() + ":"
						+ profilesForecasted.get(indexProfile).getData().get(indexData).getMinute() + ";"
						+ profilesForecasted.get(indexProfile).getData().get(indexData).getTemperature() + ";"
						+ profilesForecasted.get(indexProfile).getData().get(indexData).getLoad() + ";"
						+ profilesActual.get(indexProfile).getData().get(indexData).getLoad() + ";");
	
			}
	
		}
	
		return strings;
	
	}

}