package evaluation;

import java.util.ArrayList;

import entities.DataSet;
import entities.Profile;
import utilities.ErrorMetrics;

public class ResultMA {

	private double MAE;
	private double MAPE;
	private double NRMSE;
	private double NRMSEM;
	private double RMSE;
	private double sMAPE;
	private double pNormAve;
	private double MAEAve;
	private double MAPEAve;
	private double NRMSEAve;
	private double NRMSEMAve;
	private double RMSEAve;
	private double sMAPEAve;
	
	private ArrayList<Profile> actuals;
	private ArrayList<Profile> forecasts;

	public ResultMA(ArrayList<Profile> profilesForecasted, ArrayList<Profile> profilesActual, DataSet dataSetEval) {

		this.MAE = 0.0;
		this.MAPE = 0.0;
		this.NRMSE = 0.0;
		this.NRMSEM = 0.0;
		this.RMSE = 0.0;
		this.sMAPE = 0.0;

		for (int indexProfile = 0; indexProfile < profilesActual.size(); indexProfile++) {

			this.MAE = MAE + ErrorMetrics.calculateMeanAbsoluteError(profilesForecasted.get(indexProfile), profilesActual.get(indexProfile));
			this.MAPE = MAPE + ErrorMetrics.calculateMeanAbsolutePercentageError(profilesForecasted.get(indexProfile), profilesActual.get(indexProfile));
			this.NRMSE = NRMSE + ErrorMetrics.calculateNormalizedRootMeanSquareError(profilesForecasted.get(indexProfile), profilesActual.get(indexProfile));
			this.NRMSEM = NRMSEM
					+ ErrorMetrics.calculateNormalizedRootMeanSquareErrorByMeanOfActual(profilesForecasted.get(indexProfile), profilesActual.get(indexProfile));
			this.RMSE = RMSE + ErrorMetrics.calculateRootMeanSquareError(profilesForecasted.get(indexProfile), profilesActual.get(indexProfile));
			this.sMAPE = sMAPE
					+ ErrorMetrics.calculateSymmetricMeanAbsolutePercentageError(profilesForecasted.get(indexProfile), profilesActual.get(indexProfile));

		}
	
		this.MAEAve = this.MAE / profilesActual.size();
		this.MAPEAve = this.MAPE / profilesActual.size();
		this.NRMSEAve = this.NRMSE / profilesActual.size();
		this.NRMSEMAve = this.NRMSEM / profilesActual.size();
		this.RMSEAve = this.RMSE / profilesActual.size();
		this.sMAPEAve = this.sMAPE / profilesActual.size();
		
		this.actuals = profilesActual;
		this.forecasts = profilesForecasted;

	}

	public ArrayList<Profile> getForecasts() {
	
		return forecasts;
	
	}

	public void setForecasts(ArrayList<Profile> forecasts) {
	
		this.forecasts = forecasts;
	
	}

	public double getMAE() {

		return MAE;

	}

	public double getMAPE() {

		return MAPE;

	}

	public double getNRMSE() {

		return NRMSE;

	}

	public double getNRMSEM() {

		return NRMSEM;

	}

	public double getRMSE() {

		return RMSE;

	}

	public double getsMAPE() {

		return sMAPE;

	}

	public double getpNormAve() {

		return pNormAve;

	}

	public double getMAEAve() {

		return MAEAve;

	}

	public double getMAPEAve() {

		return MAPEAve;

	}

	public double getNRMSEAve() {

		return NRMSEAve;

	}

	public double getNRMSEMAve() {

		return NRMSEMAve;

	}

	public double getRMSEAve() {

		return RMSEAve;

	}

	public double getsMAPEAve() {

		return sMAPEAve;

	}

	@Override
	public String toString() {

		return "ResultMA [pNormAve=" + pNormAve + ", MAEAve=" + MAEAve + ", MAPEAve=" + MAPEAve + ", NRMSEAve=" + NRMSEAve
				+ ", NRMSEMAve=" + NRMSEMAve + ", RMSEAve=" + RMSEAve + ", sMAPEAve=" + sMAPEAve + "]";

	}

	public double calculatePNormAve(int omega, int p) {
		
		double pNorm = 0;
		
		for (int indexProfile = 0; indexProfile < actuals.size(); indexProfile++) {

			pNorm = pNorm + ErrorMetrics.calculateRootMeanSquareError(forecasts.get(indexProfile), actuals.get(indexProfile));

		}
		
		return pNorm / actuals.size();
		
	}

}