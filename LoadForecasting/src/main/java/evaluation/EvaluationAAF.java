package evaluation;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import entities.DataSet;
import utilities.Loader;

public class EvaluationAAF {

	static String PATH_AUSTIN_HOURLY = "src\\main\\resources\\Austin\\Hourly";
	static String PATH_AUSTIN_QUARTERLY = "src\\main\\resources\\Austin\\Quarterly";
	static String PATH_BOULDER_HOURLY = "src\\main\\resources\\Boulder\\Hourly";
	static String PATH_BOULDER_QUARTERLY = "src\\main\\resources\\Boulder\\Quarterly";

	static String path;

	public static void main(String[] args) {

		evaluate(PATH_BOULDER_HOURLY);

	}

	private static ArrayList<String> concat(String identifier, ArrayList<String> result, ArrayList<String> concats) {

		try {

			concats.set(0, concats.get(0) + ";" + identifier);

			for (int indexLine = 0; indexLine < result.size(); indexLine++) {

				concats.set(indexLine + 1, concats.get(indexLine + 1) + ";" + result.get(indexLine));

			}

		} catch (IndexOutOfBoundsException e) {

			concats.add(";" + identifier);

			for (int indexLine = 0; indexLine < result.size() / 8; indexLine++) {

				concats.add("OMEGA" + ";" + result.get(indexLine * 8 + 0));
				concats.add("P" + ";" + result.get(indexLine * 8 + 1));
				concats.add("PNORMAVE" + ";" + result.get(indexLine * 8 + 2));
				concats.add("MAEAVE" + ";" + result.get(indexLine * 8 + 3));
				concats.add("NRMSEAVE" + ";" + result.get(indexLine * 8 + 4));
				concats.add("NRMSEMAVE" + ";" + result.get(indexLine * 8 + 5));
				concats.add("RMSEAVE" + ";" + result.get(indexLine * 8 + 6));
				concats.add("SMAPEAVE" + ";" + result.get(indexLine * 8 + 7));

			}

		}

		return concats;

	}

	private static void evaluate(String pathToDir) {

		path = pathToDir;
		String location = "";
		double startTime;

		if (path.equals(PATH_AUSTIN_HOURLY) || path.equals(PATH_AUSTIN_QUARTERLY)) {

			location = "Austin";

		}

		if (path.equals(PATH_BOULDER_HOURLY) || path.equals(PATH_BOULDER_QUARTERLY)) {

			location = "Boulder";

		}

		ArrayList<DataSet> dataSets = Loader.loadDataSets(path);
		ArrayList<ArrayList<String>> result = new ArrayList<ArrayList<String>>();
		ArrayList<String> concats = new ArrayList<String>();

		for (int selector = 0; selector < dataSets.size(); selector++) {

			System.out.println(dataSets.get(selector).getIdentifier());
			startTime = System.currentTimeMillis();
			result = toString(dataSets.get(selector).getIdentifier(), evaluate(location, dataSets.get(selector)));
			print("\\Forecasts\\AAF\\AAF_Forecasts_" + dataSets.get(selector).getIdentifier(), result.get(1));
			concats = concat(dataSets.get(selector).getIdentifier(), result.get(0), concats);
			System.out.println("DURATION " + ((System.currentTimeMillis() - startTime) / 1000.0) + "sec");

		}

		print("\\Forecasts\\AAF\\AAF_ErrorMetrics", concats);

	}

	private static ArrayList<ResultAAF> evaluate(String location, DataSet dataSet) {

		int[] ps = new int[] { 1, 2, 4 };
		int[] omegas = new int[] { 0, 1, 2, 4, 8, 12 };

		ExecutorService executor = Executors.newFixedThreadPool(18);
		ArrayList<Future<ResultAAF>> resultsTestingFuture = new ArrayList<Future<ResultAAF>>();

		for (int omega : omegas) {

			for (int p : ps) {

				synchronized (resultsTestingFuture) {

					resultsTestingFuture.add(executor.submit(new EvaluatorAAF(omega, p, dataSet)));

				}

			}

		}

		executor.shutdown();

		try {

			executor.awaitTermination(3, TimeUnit.HOURS);

		} catch (InterruptedException e) {

			e.printStackTrace();

		}

		ArrayList<ResultAAF> resultsTesting = new ArrayList<ResultAAF>();

		for (int indexResult = 0; indexResult < (ps.length * omegas.length); indexResult++) {

			try {

				resultsTesting.add(resultsTestingFuture.get(indexResult).get());

			} catch (InterruptedException | ExecutionException e) {

				e.printStackTrace();

			}

		}

		resultsTesting.sort(new Comparator<ResultAAF>() {

			@Override
			public int compare(ResultAAF o1, ResultAAF o2) {

				if (o1.getOmega() > o2.getOmega()) {

					return 1;

				}

				if (o1.getOmega() < o2.getOmega()) {

					return -1;

				}

				if (o1.getOmega() == o2.getOmega()) {

					if (o1.getP() > o2.getP()) {

						return 1;

					}

					if (o1.getP() < o2.getP()) {

						return -1;

					}

				}

				return 0;

			}

		});

		return resultsTesting;

	}

	private static void print(String target, ArrayList<String> lines) {

		BufferedWriter bufferedWriter;

		try {

			bufferedWriter = new BufferedWriter(new FileWriter(path + target + ".csv"));

			for (int indexLine = 0; indexLine < lines.size(); indexLine++) {

				bufferedWriter.write(lines.get(indexLine) + "\n");

			}

			bufferedWriter.close();

		} catch (IOException e) {

			e.printStackTrace();

		}

	}

	private static ArrayList<ArrayList<String>> toString(String identifier, ArrayList<ResultAAF> resultsAAF) {

		ArrayList<ArrayList<String>> result = new ArrayList<ArrayList<String>>();

		ArrayList<String> errorMetrics = new ArrayList<String>();
		ArrayList<String> forecasts = new ArrayList<String>();

		for (int indexResult = 0; indexResult < resultsAAF.size(); indexResult++) {

			errorMetrics.add(String.valueOf(resultsAAF.get(indexResult).getOmega()));
			errorMetrics.add(String.valueOf(resultsAAF.get(indexResult).getP()));
			errorMetrics.add(String.valueOf(resultsAAF.get(indexResult).getpNormAve()));
			errorMetrics.add(String.valueOf(resultsAAF.get(indexResult).getMAEAve()));
			errorMetrics.add(String.valueOf(resultsAAF.get(indexResult).getNRMSEAve()));
			errorMetrics.add(String.valueOf(resultsAAF.get(indexResult).getNRMSEM()));
			errorMetrics.add(String.valueOf(resultsAAF.get(indexResult).getRMSEAve()));
			errorMetrics.add(String.valueOf(resultsAAF.get(indexResult).getsMAPEAve()));

			try {

				forecasts.set(0, forecasts.get(0) + ";" + resultsAAF.get(indexResult).getOmega() + " "
						+ resultsAAF.get(indexResult).getP());

			} catch (IndexOutOfBoundsException e) {

				forecasts.add(
						"Date;" + resultsAAF.get(indexResult).getOmega() + " " + resultsAAF.get(indexResult).getP());

			}

			for (int indexProfile = 0; indexProfile < resultsAAF.get(indexResult).getForecasts()
					.size(); indexProfile++) {

				for (int indexData = 0; indexData < resultsAAF.get(indexResult).getForecasts().get(indexProfile)
						.getData().size(); indexData++) {

					try {

						forecasts.set(1 + indexData + 24 * indexProfile,
								forecasts.get(1 + indexData + 24 * indexProfile) + ";"
										+ String.valueOf(resultsAAF.get(indexResult).getForecasts().get(indexProfile)
												.getData().get(indexData).getLoad()));

					} catch (IndexOutOfBoundsException e) {

						forecasts.add(resultsAAF.get(indexResult).getForecasts().get(indexProfile).getDateTime()
								.getDayOfMonth() + "-"
								+ resultsAAF.get(indexResult).getForecasts().get(indexProfile).getDateTime().getMonth()
								+ "-"
								+ resultsAAF.get(indexResult).getForecasts().get(indexProfile).getDateTime().getYear()
								+ "T"
								+ resultsAAF.get(indexResult).getForecasts().get(indexProfile).getData().get(indexData)
										.getHour()
								+ ":"
								+ resultsAAF.get(indexResult).getForecasts().get(indexProfile).getData().get(indexData)
										.getMinute()
								+ ";" + String.valueOf(resultsAAF.get(indexResult).getForecasts().get(indexProfile)
										.getData().get(indexData).getLoad()));

					}

				}

			}

		}

		result.add(errorMetrics);
		result.add(forecasts);

		return result;

	}

}