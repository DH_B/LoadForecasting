package evaluation;

import java.util.ArrayList;

import entities.Data;
import entities.DataSet;
import entities.Profile;
import utilities.ErrorMetrics;

public class ResultAAF {

	private int omega;
	private int p;

	private double pNorm;
	private double MAE;
	private double MAPE;
	private double NRMSE;
	private double NRMSEM;
	private double RMSE;
	private double sMAPE;
	private double pNormAve;
	private double MAEAve;
	private double MAPEAve;
	private double NRMSEAve;
	private double NRMSEMAve;
	private double RMSEAve;
	private double sMAPEAve;
	
	private ArrayList<Profile> forecasts;

	public ResultAAF(int omega, int p, ArrayList<Profile> profilesForecasted, ArrayList<Profile> profilesActual, DataSet dataSetEval) {

		this.omega = omega;
		this.p = p;

		this.pNorm = 0.0;
		this.MAE = 0.0;
		this.MAPE = 0.0;
		this.NRMSE = 0.0;
		this.NRMSEM = 0.0;
		this.RMSE = 0.0;
		this.sMAPE = 0.0;

		for (int indexProfile = 0; indexProfile < profilesActual.size(); indexProfile++) {

			this.pNorm = pNorm + ErrorMetrics.calculatePNorm(omega, p, profilesForecasted.get(indexProfile), profilesActual.get(indexProfile));
			this.MAE = MAE + ErrorMetrics.calculateMeanAbsoluteError(profilesForecasted.get(indexProfile), profilesActual.get(indexProfile));
			this.MAPE = MAPE + ErrorMetrics.calculateMeanAbsolutePercentageError(profilesForecasted.get(indexProfile), profilesActual.get(indexProfile));
			this.NRMSE = NRMSE + ErrorMetrics.calculateNormalizedRootMeanSquareError(profilesForecasted.get(indexProfile), profilesActual.get(indexProfile));
			this.NRMSEM = NRMSEM
					+ ErrorMetrics.calculateNormalizedRootMeanSquareErrorByMeanOfActual(profilesForecasted.get(indexProfile), profilesActual.get(indexProfile));
			this.RMSE = RMSE + ErrorMetrics.calculateRootMeanSquareError(profilesForecasted.get(indexProfile), profilesActual.get(indexProfile));
			this.sMAPE = sMAPE
					+ ErrorMetrics.calculateSymmetricMeanAbsolutePercentageError(profilesForecasted.get(indexProfile), profilesActual.get(indexProfile));

		}

		this.pNormAve = this.pNorm / profilesActual.size();
		this.MAEAve = this.MAE / profilesActual.size();
		this.MAPEAve = this.MAPE / profilesActual.size();
		this.NRMSEAve = this.NRMSE / profilesActual.size();
		this.NRMSEMAve = this.NRMSEM / profilesActual.size();
		this.RMSEAve = this.RMSE / profilesActual.size();
		this.sMAPEAve = this.sMAPE / profilesActual.size();
		
		for (Profile profile : profilesForecasted) {
			
			if ((profile.getDateTime().getDayOfMonth() == 8 && profile.getDateTime().getMonth() == 3
					&& profile.getDateTime().getYear() == 2015)
					|| (profile.getDateTime().getDayOfMonth() == 13 && profile.getDateTime().getMonth() == 3)
							&& profile.getDateTime().getYear() == 2016) {

				profile.getData().remove(1);

				for (int indexData = 0; indexData < profile.getData().size(); indexData++) {
					
					profile.getData().get(indexData).setHour(indexData);
					
				}
				
				profile.calculateMeans();

			}

			if ((profile.getDateTime().getDayOfMonth() == 1 && profile.getDateTime().getMonth() == 11
					&& profile.getDateTime().getYear() == 2015)
					|| (profile.getDateTime().getDayOfMonth() == 6 && profile.getDateTime().getMonth() == 11)
							&& profile.getDateTime().getYear() == 2016) {
				
				Data data = new Data();
				data.setMinute(0);
				
				data.setLoad((profile.getData().get(1).getLoad() + profile.getData().get(2).getLoad()) / 2);
				data.setTemperature(
						(profile.getData().get(1).getTemperature() + profile.getData().get(2).getTemperature()) / 2);
				profile.getData().add(2, data);

				for (int indexData = 0; indexData < profile.getData().size(); indexData++) {

					profile.getData().get(indexData).setHour(indexData);

				}
				
				profile.calculateMeans();
				
			}
			
		}

		this.forecasts = profilesForecasted;
		
		System.out.println(this);
		
	}

	public ArrayList<Profile> getForecasts() {
	
		return forecasts;
	
	}

	public void setForecasts(ArrayList<Profile> forecasts) {

		this.forecasts = forecasts;
	
	}

	public int getOmega() {

		return omega;

	}

	public void setOmega(int omega) {

		this.omega = omega;

	}

	public int getP() {

		return p;

	}

	public void setP(int p) {

		this.p = p;

	}

	public double getpNorm() {

		return pNorm;

	}

	public double getMAE() {

		return MAE;

	}

	public double getMAPE() {

		return MAPE;

	}

	public double getNRMSE() {

		return NRMSE;

	}

	public double getNRMSEM() {

		return NRMSEM;

	}

	public double getRMSE() {

		return RMSE;

	}

	public double getsMAPE() {

		return sMAPE;

	}

	public double getpNormAve() {

		return pNormAve;

	}

	public double getMAEAve() {

		return MAEAve;

	}

	public double getMAPEAve() {

		return MAPEAve;

	}

	public double getNRMSEAve() {

		return NRMSEAve;

	}

	public double getNRMSEMAve() {

		return NRMSEMAve;

	}

	public double getRMSEAve() {

		return RMSEAve;

	}

	public double getsMAPEAve() {

		return sMAPEAve;

	}

	@Override
	public String toString() {

		return "ResultAAF [omega=" + omega + ", p=" + p + ", pNormAve=" + pNormAve + ", MAEAve=" + MAEAve + ", MAPEAve=" + MAPEAve + ", NRMSEAve=" + NRMSEAve
				+ ", NRMSEMAve=" + NRMSEMAve + ", RMSEAve=" + RMSEAve + ", sMAPEAve=" + sMAPEAve + "]";

	}

}