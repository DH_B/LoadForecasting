package entities;

import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import de.jollyday.Holiday;
import de.jollyday.HolidayCalendar;
import de.jollyday.HolidayManager;
import de.jollyday.ManagerParameter;
import de.jollyday.ManagerParameters;
import utilities.Calculator;
import utilities.ErrorMetrics;

public class DataSet {

	private int resolution;
	private ArrayList<Profile> profiles;
	private String identifier;

	public DataSet() {

	}

	public DataSet(int resolution, ArrayList<Profile> profiles) {

		this.resolution = resolution;
		this.profiles = profiles;

	}

	public class Result {

		private int indexProfile;
		private double pNorm;

		public Result(int indexProfile, double pNorm) {

			this.indexProfile = indexProfile;
			this.pNorm = pNorm;

		}

		public int getIndexProfile() {

			return indexProfile;

		}

		public double getPNorm() {

			return pNorm;

		}

	}

	public Profile calculateBaseProfile(Profile actual) {

		return Calculator.calculateBaseProfile(actual, this);

	}

	public int getResolution() {

		return resolution;

	}

	public DataSet getCopy() {

		DataSet copy = new DataSet(resolution, new ArrayList<Profile>());

		for (Profile profile : profiles) {

			copy.getProfiles().add(profile.getCopy());

		}

		return copy;

	}
	
	public DataSet getDataSetBefore(int numProfiles, Profile actual) {
		
		int dayOfMonth = actual.getDateTime().getDayOfMonth();
		int month = actual.getDateTime().getMonth();
		int year = actual.getDateTime().getYear();
		DayOfWeek dayOfWeek = getDayOfWeek(dayOfMonth, month, year);
		DataSet dataSetBefore = new DataSet(resolution, new ArrayList<Profile>());
		
		for (Profile profile : profiles) {
			
			if (profile.getDateTime().isSameDayOfWeek(dayOfWeek) && profile.getDateTime().isBefore(dayOfMonth, month, year)) {
				
				dataSetBefore.getProfiles().add(profile);
				
			}
			
		}
		
		dataSetBefore = dataSetBefore.getDataSetLast(numProfiles);
		
		return dataSetBefore;
		
	}
	
	public DataSet getDataSetBefore(Integer dayOfMonth, Integer month, Integer year) {
	
		ArrayList<Profile> profilesBefore = new ArrayList<Profile>();
	
		for (Profile profile : profiles) {
	
			if (profile.getDateTime().isBefore(dayOfMonth, month, year)) {
	
				profilesBefore.add(profile);
	
			}
	
		}
	
		return new DataSet(resolution, profilesBefore);
	
	}
	
	public DataSet getDataSetByDayOfWeek(DayOfWeek dayOfWeek) {

		ArrayList<Profile> profilesSameDay = new ArrayList<Profile>();

		for (Profile profile : profiles) {

			if (profile.getDateTime().isSameDayOfWeek(dayOfWeek)) {

				profilesSameDay.add(profile);

			}

		}

		return new DataSet(resolution, profilesSameDay);

	}

	public DataSet getDataSetEval() {
	
		DataSet dataSetEval = new DataSet(resolution, new ArrayList<Profile>());
	
		for (Profile profile : profiles) {
	
			if (!profile.getDateTime().isBefore(1, 7, 2016)) {
	
				dataSetEval.getProfiles().add(profile);
	
			}
	
		}
	
		return dataSetEval;
	
	}

	public DataSet getDataSetHolidays(String location) { // with sundays

		if (location.equals("Austin")) {

			location = "tx";

		}

		if (location.equals("Boulder")) {

			location = "co";

		}

		ArrayList<Profile> profilesHolidays = new ArrayList<Profile>();

		ManagerParameter managerParameter = ManagerParameters.create(HolidayCalendar.UNITED_STATES);
		HolidayManager holidayManager = HolidayManager.getInstance(managerParameter);
		Set<Holiday> holidays = holidayManager.getHolidays(profiles.get(0).getDateTime().getYear(), location);

		for (int year = profiles.get(0).getDateTime().getYear() + 1; year <= profiles.get(profiles.size() - 1)
				.getDateTime().getYear(); year++) {

			holidays.addAll(holidayManager.getHolidays(year, location));

		}

		Iterator<Holiday> iterator = holidays.iterator();
		Profile profile = new Profile();

		for (int i = 0; i < holidays.size(); i++) {

			Holiday holiday = iterator.next();
			Integer year = holiday.getDate().getYear();
			Integer month = holiday.getDate().getMonthValue();
			Integer day = holiday.getDate().getDayOfMonth();
			profile = getProfileByDate(day, month, year);

			if (profile != null && profile.getDateTime().getDayOfWeek().compareTo(DayOfWeek.SUNDAY) != 0) {

				profilesHolidays.add(profile);

			}

		}

		DataSet dataSetHolidays = getDataSetByDayOfWeek(DayOfWeek.SUNDAY);
		dataSetHolidays.getProfiles().addAll(profilesHolidays);

		return dataSetHolidays;

	}

	public DataSet getDataSetLast(int i) {
	
		DataSet dataSet = new DataSet(resolution, new ArrayList<Profile>());
		
		for (int indexProfile = profiles.size() - 1; indexProfile > profiles.size() - 7; indexProfile--) {
	
			try {
	
				dataSet.getProfiles().add(profiles.get(indexProfile));
	
			} catch (IndexOutOfBoundsException e) {
	
				break;
	
			}
	
		}
	
		return dataSet;
	
	}

	public DataSet getDataSetNearestNeighbors(int k, Profile actual) {
	
		DataSet dataSetNearestNeighbors = new DataSet(resolution, new ArrayList<Profile>());
	
		if (profiles.isEmpty()) {
	
			return dataSetNearestNeighbors;
	
		}
	
		ArrayList<Result> results = new ArrayList<Result>();
	
		for (int indexProfile = 0; indexProfile < profiles.size(); indexProfile++) {
	
			results.add(new Result(indexProfile,
					ErrorMetrics.calculateNormalizedRootMeanSquareErrorByTemperature(profiles.get(indexProfile), actual)));
	
		}
	
		System.setProperty("java.util.Arrays.useLegacyMergeSort", "true");
	
		results.sort(new Comparator<Result>() {
	
			public int compare(Result r1, Result r2) {
	
				if (r1.getPNorm() > r2.getPNorm()) {
	
					return 1;
	
				}
	
				if (r1.getPNorm() < r2.getPNorm()) {
	
					return -1;
	
				}
	
				return 0;
	
			}
	
		});
	
		if (k > results.size()) {
	
			k = results.size();
	
		}
	
		List<Result> resultsSelected = results.subList(0, k);
	
		for (Result resultSelected : resultsSelected) {
	
			dataSetNearestNeighbors.getProfiles().add(profiles.get(resultSelected.getIndexProfile()).getCopy());
	
		}
	
		dataSetNearestNeighbors.getProfiles().sort(new Comparator<Profile>() {
	
			public int compare(Profile p1, Profile p2) {
	
				if (!p1.getDateTime().isBefore(p2.getDateTime().getDayOfMonth(), p2.getDateTime().getMonth(),
						p2.getDateTime().getYear())) {
	
					return 1;
	
				}
	
				if (p1.getDateTime().isBefore(p2.getDateTime().getDayOfMonth(), p2.getDateTime().getMonth(),
						p2.getDateTime().getYear())) {
	
					return -1;
	
				}
	
				return 0;
	
			}
	
		});
	
		return dataSetNearestNeighbors;
	
	}

	public DataSet getDataSetTesting() {
	
		DataSet dataSetTesting = new DataSet(resolution, new ArrayList<Profile>());
	
		for (Profile profile : profiles) {
	
			if (!profile.getDateTime().isBefore(1, 1, 2016) && profile.getDateTime().isBefore(1, 7, 2016)) {
	
				dataSetTesting.getProfiles().add(profile);
	
			}
	
		}
	
		return dataSetTesting;
	
	}

	public DataSet getDataSetTraining() {

		DataSet dataSetTraining = new DataSet(resolution, new ArrayList<Profile>());

		for (Profile profile : profiles) {

			if (profile.getDateTime().isBefore(1, 7, 2016)) {

				dataSetTraining.getProfiles().add(profile);

			}

		}

		return dataSetTraining;

	}

	public DataSet getDataSetWithoutHolidays(String location) {

		ArrayList<Profile> profilesWithoutHolidays = new ArrayList<Profile>();
		
		for (Profile profile : profiles) {
			
			if (!isHoliday(profile.getDateTime().getDayOfMonth(), profile.getDateTime().getMonth(), profile.getDateTime().getYear(), location)) {
				
				profilesWithoutHolidays.add(profile);
				
			}
			
		}
		
		return new DataSet(resolution, profilesWithoutHolidays);

	}

	public DayOfWeek getDayOfWeek(Integer dayOfMonth, Integer month, Integer year) {

		for (Profile profile : profiles) {

			if (profile.getDateTime().isSameDate(dayOfMonth, month, year)) {

				return profile.getDateTime().getDayOfWeek();

			}

		}

		return null;

	}

	public ArrayList<DataSet> getFolds() {

		ArrayList<DataSet> folds = new ArrayList<DataSet>();

		DataSet fold_1 = new DataSet(resolution, new ArrayList<Profile>());
		DataSet fold_2 = new DataSet(resolution, new ArrayList<Profile>());
		DataSet fold_3 = new DataSet(resolution, new ArrayList<Profile>());
		DataSet fold_4 = new DataSet(resolution, new ArrayList<Profile>());
		DataSet fold_5 = new DataSet(resolution, new ArrayList<Profile>());
		DataSet fold_6 = new DataSet(resolution, new ArrayList<Profile>());

		for (Profile profile : profiles) {

			switch (profile.getDateTime().getMonth()) {

			case 1:

				fold_1.getProfiles().add(profile);
				break;

			case 2:

				fold_2.getProfiles().add(profile);
				break;

			case 3:

				fold_3.getProfiles().add(profile);
				break;

			case 4:

				fold_4.getProfiles().add(profile);
				break;

			case 5:

				fold_5.getProfiles().add(profile);
				break;

			case 6:

				fold_6.getProfiles().add(profile);
				break;

			}

		}

		folds.add(fold_1);
		folds.add(fold_2);
		folds.add(fold_3);
		folds.add(fold_4);
		folds.add(fold_5);
		folds.add(fold_6);

		return folds;

	}

	private ArrayList<Profile> getHolidays(String location) {

		if (location.equals("Austin")) {

			location = "tx";

		}

		if (location.equals("Boulder")) {

			location = "co";

		}

		Profile profile = new Profile();
		ArrayList<Profile> profilesHoliday = new ArrayList<Profile>();

		ManagerParameter managerParameter = ManagerParameters.create(HolidayCalendar.UNITED_STATES);
		HolidayManager holidayManager = HolidayManager.getInstance(managerParameter);
		Set<Holiday> holidays = holidayManager.getHolidays(this.getProfiles().get(0).getDateTime().getYear(), location);

		for (int year = this.getProfiles().get(0).getDateTime().getYear() + 1; year <= this.getProfiles()
				.get(this.getProfiles().size() - 1).getDateTime().getYear(); year++) {

			holidays.addAll(holidayManager.getHolidays(year, location));

		}

		Iterator<Holiday> iterator = holidays.iterator();

		for (int i = 0; i < holidays.size(); i++) {

			Holiday holiday = iterator.next();
			Integer year = holiday.getDate().getYear();
			Integer month = holiday.getDate().getMonthValue();
			Integer day = holiday.getDate().getDayOfMonth();
			profile = getProfileByDate(day, month, year);

			if (profile != null) {

				profilesHoliday.add(profile);

			}

		}

		return profilesHoliday;

	}

	public String getIdentifier() {

		return identifier;

	}

	public ArrayList<Profile> getProfiles() {

		return profiles;

	}

	public Profile getProfileByDate(Integer dayOfMonth, Integer month, Integer year) {

		for (Profile profile : profiles) {

			if (profile.getDateTime().isSameDate(dayOfMonth, month, year)) {

				return profile;

			}

		}

		return null;

	}

	public void handleTimeChange() {

		for (Profile profile : profiles) {

			if ((profile.getDateTime().getDayOfMonth() == 8 && profile.getDateTime().getMonth() == 3
					&& profile.getDateTime().getYear() == 2015)
					|| (profile.getDateTime().getDayOfMonth() == 13 && profile.getDateTime().getMonth() == 3)
							&& profile.getDateTime().getYear() == 2016) {

				Data data = new Data();
				data.setMinute(0);

				data.setLoad((profile.getData().get(1).getLoad() + profile.getData().get(2).getLoad()) / 2);
				data.setTemperature(
						(profile.getData().get(1).getTemperature() + profile.getData().get(2).getTemperature()) / 2);
				profile.getData().add(2, data);

				for (int indexData = 0; indexData < profile.getData().size(); indexData++) {

					profile.getData().get(indexData).setHour(indexData);

				}

				profile.calculateMeans();

			}

			if ((profile.getDateTime().getDayOfMonth() == 1 && profile.getDateTime().getMonth() == 11
					&& profile.getDateTime().getYear() == 2015)
					|| (profile.getDateTime().getDayOfMonth() == 6 && profile.getDateTime().getMonth() == 11)
							&& profile.getDateTime().getYear() == 2016) {

				profile.getData().remove(1);

				for (int indexData = 0; indexData < profile.getData().size(); indexData++) {

					profile.getData().get(indexData).setHour(indexData);

				}

				profile.calculateMeans();

			}

		}

	}

	public boolean isHoliday(Integer dayOfMonth, Integer month, Integer year, String location) {

		ArrayList<Profile> profilesHoliday = getHolidays(location);
		Profile profile = getProfileByDate(dayOfMonth, month, year);

		if (profilesHoliday.contains(profile)) {

			return true;

		}

		return false;

	}

	public void setIdentifier(String identifier) {

		this.identifier = identifier;

	}

	public void setProfiles(ArrayList<Profile> profiles) {

		this.profiles = profiles;

	}

	public void setResolution(int resolution) {

		this.resolution = resolution;

	}

}