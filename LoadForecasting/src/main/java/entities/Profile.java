package entities;

import java.util.ArrayList;

public class Profile {

	private ArrayList<Data> data;
	private DateTime dateTime;
	private double loadMean;
	private double temperatureMean;

	public void calculateMeans() {

		this.loadMean = 0.0;
		this.temperatureMean = 0.0;

		for (Data dataPoint : this.data) {

			this.loadMean = this.loadMean + dataPoint.getLoad();
			this.temperatureMean = this.temperatureMean + dataPoint.getTemperature();

		}

		this.loadMean = this.loadMean / this.data.size();
		this.temperatureMean = this.temperatureMean / this.data.size();

	}

	public Profile getCopy() {

		Profile copyProfile = new Profile();
		ArrayList<Data> copyData = new ArrayList<Data>();

		for (Data data : data) {

			copyData.add(data.getCopy());

		}

		copyProfile.setData(copyData);
		copyProfile.setDateTime(dateTime.getCopy());
		copyProfile.setLoadMean(loadMean);
		copyProfile.setTemperatureMean(temperatureMean);

		return copyProfile;

	}

	public ArrayList<Data> getData() {

		return data;

	}

	public DateTime getDateTime() {

		return dateTime;

	}

	public double getLoadMean() {

		return loadMean;

	}

	public double getTemperatureMean() {

		return temperatureMean;

	}

	public void setData(ArrayList<Data> data) {

		this.data = data;

	}

	public void setDateTime(DateTime dateTime) {

		this.dateTime = dateTime;

	}

	public void setLoadMean(double loadMean) {

		this.loadMean = loadMean;

	}

	public void setTemperatureMean(double temperatureMean) {

		this.temperatureMean = temperatureMean;

	}

	@Override
	public String toString() {

		return "Profile [data=" + data + ", dateTime=" + dateTime + ", loadMean=" + loadMean + ", temperatureMean=" + temperatureMean + "]";

	}

}