package entities;

import java.time.DayOfWeek;

public class DateTime {

	private int dayOfMonth;
	private int month;
	private int year;
	private DayOfWeek dayOfWeek;

	public DateTime getCopy() {
		
		DateTime copy = new DateTime();
		
		copy.setDayOfMonth(dayOfMonth);
		copy.setMonth(month);
		copy.setYear(year);
		copy.setDayOfWeek(dayOfWeek);
		
		return copy;
		
	}

	public DayOfWeek getDayOfWeek() {

		return dayOfWeek;

	}

	public int getDayOfMonth() {

		return dayOfMonth;

	}

	public int getMonth() {
	
		return month;
	
	}

	public int getYear() {
	
		return year;
	
	}

	public boolean isBefore(Integer dayOfMonth, Integer month, Integer year) {
	
		boolean isBefore = false;
	
		if (this.year == year) {
	
			if (this.month == month) {
	
				if (this.dayOfMonth < dayOfMonth) {
	
					isBefore = true;
	
				}
	
			}
	
			if (this.month < month) {
	
				isBefore = true;
	
			}
	
		}
	
		if (this.year < year) {
	
			isBefore = true;
	
		}
	
		return isBefore;
	
	}

	public boolean isSameDate(int dayOfMonth, int month, int year) {
	
		if (this.dayOfMonth == dayOfMonth && this.month == month && this.year == year) {
	
			return true;
	
		}
	
		return false;
	
	}

	public boolean isSameDayOfWeek(DayOfWeek dayOfWeek) {
	
		if (this.dayOfWeek == dayOfWeek) {
	
			return true;
	
		}
	
		return false;
	
	}

	public void setDayOfMonth(int dayOfMonth) {

		this.dayOfMonth = dayOfMonth;

	}

	public void setDayOfWeek(DayOfWeek dayOfWeek) {
	
		this.dayOfWeek = dayOfWeek;
	
	}

	public void setMonth(int month) {

		this.month = month;

	}

	public void setYear(int year) {

		this.year = year;

	}

	@Override
	public String toString() {
	
		return "DateTime [dayOfMonth = " + dayOfMonth + ", month = " + month + ", year = " + year + ", dayOfWeek = " + dayOfWeek + "]";
	
	}

}
