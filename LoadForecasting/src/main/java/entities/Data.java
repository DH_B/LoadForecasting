package entities;

public class Data {

	private int hour;
	private int minute;
	private double load;
	private double temperature;

	public Data getCopy() {
		
		Data copy = new Data();
		copy.setHour(hour);
		copy.setMinute(minute);
		copy.setLoad(load);
		copy.setTemperature(temperature);
		
		return copy;
	
	}

	public int getHour() {
	
		return hour;
	
	}

	public double getLoad() {
	
		return load;
	
	}

	public int getMinute() {
		
		return minute;
		
	}

	public double getTemperature() {
	
		return temperature;
	
	}

	public void setHour(int hour) {

		this.hour = hour;
	
	}

	public void setLoad(double load) {

		this.load = load;

	}

	public void setMinute(int minute) {
		
		this.minute = minute;
		
	}

	public void setTemperature(double temperature) {

		this.temperature = temperature;

	}

	@Override
	public String toString() {
	
		return "Data [hour=" + hour + ", minute=" + minute + ", load=" + (double) (((int) (load * 100.0)) / 100.0) + ", temperature=" + temperature + "]";
	
	}

}