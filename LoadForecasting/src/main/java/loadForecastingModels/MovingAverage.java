package loadForecastingModels;

import java.util.Arrays;

import entities.Data;
import entities.DataSet;
import entities.Profile;

public class MovingAverage {

	private Data prediction;
	private DataSet dataSet;

	public MovingAverage(DataSet dataSet) {

		this.prediction = new Data();
		this.dataSet = dataSet;

	}

	public void calculatePrediction(int step, Profile actual) {

		DataSet dataSetMA = dataSet.getDataSetBefore(6, actual);

		if (!dataSetMA.getProfiles().isEmpty()) {

			double[] values = new double[6];
			
			for (int i = 0; i < 6; i++) {
				
				values[i] = dataSetMA.getProfiles().get(i).getData().get(step).getLoad();
				
			}
			
			Arrays.sort(values);
			
			double average = 0.0;
			
			for (int indexProfile = 1; indexProfile < values.length - 1; indexProfile++) {

				average = average + values[indexProfile];

			}
			
			average = average / (dataSetMA.getProfiles().size() - 2);
			
			prediction.setHour(actual.getData().get(step).getHour());
			prediction.setLoad(average);
			prediction.setMinute(actual.getData().get(step).getMinute());
			prediction.setTemperature(actual.getData().get(step).getTemperature());
			
		} else {
			
			prediction = null;
			
		}

	}

	public Data getPrediction() {
		
		return prediction;
		
	}

}