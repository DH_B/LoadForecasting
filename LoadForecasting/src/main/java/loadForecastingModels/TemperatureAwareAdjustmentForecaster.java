package loadForecastingModels;

import java.time.DayOfWeek;

import entities.DataSet;
import entities.Profile;
import linearAssignmentProblem.LinearAssignmentProblem;

public class TemperatureAwareAdjustmentForecaster {
	
	private int omega;
	private int p;
	private int k;
	private double baseTemperature;
	private String location;
	private DataSet dataSet;
	private Profile prediction;

	public TemperatureAwareAdjustmentForecaster(int omega, int p, int k, double baseTemperature, String location, DataSet dataSet) {

		this.omega = omega;
		this.p = p;
		this.k = k;
		this.baseTemperature = baseTemperature;
		this.location = location;
		this.dataSet = dataSet;
		this.prediction = new Profile();

	}
	
	public void calculatePrediction(Profile actual) {
		
		int dayOfMonth = actual.getDateTime().getDayOfMonth();
		int month = actual.getDateTime().getMonth();
		int year = actual.getDateTime().getYear();
		
		DayOfWeek dayOfWeek = dataSet.getDayOfWeek(dayOfMonth, month, year);
		DataSet dataSet;

		if (this.dataSet.isHoliday(dayOfMonth, month, year, location)) {
			
			dataSet = this.dataSet.getDataSetHolidays(location);

		} else {

			dataSet = this.dataSet.getDataSetByDayOfWeek(dayOfWeek);
			dataSet = dataSet.getDataSetWithoutHolidays(location);

		}

		dataSet = dataSet.getDataSetBefore(dayOfMonth, month, year);
		dataSet = dataSet.getDataSetNearestNeighbors(k, actual);
		
		if (!dataSet.getProfiles().isEmpty()) {
			
			prediction = dataSet.calculateBaseProfile(actual);

			for (int i = dataSet.getProfiles().size() - 1; i >= 0; i--) {
				
				updateBaseProfile((double) (dataSet.getProfiles().size() - i), dataSet.getProfiles().get(i));

			}

			prediction.setDateTime(actual.getDateTime());
			prediction.calculateMeans();
				
		} else {
			
			prediction = null;
			
		}
		
	}
	
	private void calculateUpdate(double iteration, Profile permutedProfile) {

		double result;

		for (int i = 0; i < permutedProfile.getData().size(); i++) {

			result = ((prediction.getData().get(i).getLoad() * iteration) + permutedProfile.getData().get(i).getLoad()) * ((1.0 / (1.0 + iteration)));
			prediction.getData().get(i).setLoad(result);

		}

	}

	public int getOmega() {
	
		return omega;
	
	}

	public int getP() {
	
		return p;
	
	}

	public int getK() {
	
		return k;
	
	}

	public double getBaseTemperature() {
	
		return baseTemperature;
	
	}

	public DataSet getDataSet() {
	
		return dataSet;
	
	}

	public Profile getPrediction() {
	
		return prediction;
	
	}

	public void setOmega(int omega) {
	
		this.omega = omega;
	
	}

	public void setP(int p) {
	
		this.p = p;
	
	}

	public void setK(int k) {
	
		this.k = k;
	
	}

	public void setBaseTemperature(double baseTemperature) {
	
		this.baseTemperature = baseTemperature;
	
	}

	public void setDataSet(DataSet dataSet) {
	
		this.dataSet = dataSet;
	
	}

	public void setPrediction(Profile prediction) {
	
		this.prediction = prediction;
	
	}

	@Override
	public String toString() {
	
		return "TemperatureAwareAdjustmentForecaster [omega=" + omega + ", p=" + p + ", k=" + k + ", baseTemperature=" + baseTemperature + ", dataSet="
				+ dataSet + ", prediction=" + prediction + "]";
	
	}
	
	private void updateBaseProfile(double iteration, Profile profile) {

		LinearAssignmentProblem linAssProblem = new LinearAssignmentProblem(omega, p, 0, profile, prediction);
		Profile permutedProfile = linAssProblem.solve();
		calculateUpdate(iteration, permutedProfile);

	}

}
