package loadForecastingModels;

import java.time.DayOfWeek;

import entities.DataSet;
import entities.Profile;
import linearAssignmentProblem.LinearAssignmentProblem;

public class AveragedAdjustmentForecaster {

	private int omega;
	private int p;
	private DataSet dataSet;
	private Profile prediction;

	public AveragedAdjustmentForecaster(int omega, int p, DataSet dataSet) {
		
		this.omega = omega;
		this.p = p;
		this.dataSet = dataSet;
		this.prediction = new Profile();

	}

	public void calculatePrediction(Profile actual) {
		
		int dayOfMonth = actual.getDateTime().getDayOfMonth();
		int month = actual.getDateTime().getMonth();
		int year = actual.getDateTime().getYear();

		DayOfWeek dayOfWeekPrediction = dataSet.getDayOfWeek(dayOfMonth, month, year);
		DataSet dataSet = this.dataSet.getDataSetByDayOfWeek(dayOfWeekPrediction);
		dataSet = dataSet.getDataSetBefore(dayOfMonth, month, year);

		if (!dataSet.getProfiles().isEmpty()) {

			prediction = dataSet.calculateBaseProfile(actual);
			
			for (int i = dataSet.getProfiles().size() - 1; i >= 0; i--) {

				updateBaseProfile((double) (dataSet.getProfiles().size() - i), dataSet.getProfiles().get(i));

			}

			prediction.setDateTime(actual.getDateTime());
			prediction.calculateMeans();

		} else {

			prediction = null;

		}

	}
	
	private void calculateUpdate(double iteration, Profile permutedProfile) {

		double result;

		for (int i = 0; i < permutedProfile.getData().size(); i++) {

			result = ((this.prediction.getData().get(i).getLoad() * iteration) + permutedProfile.getData().get(i).getLoad()) * ((1.0 / (1.0 + iteration)));
			this.prediction.getData().get(i).setLoad(result);

		}

	}

	public Profile getPrediction() {
		
		return prediction;
		
	}

	@Override
	public String toString() {
	
		return "AveragedAdjustmentForecaster [omega=" + omega + ", p=" + p + ", dataSet=" + dataSet + ", prediction=" + prediction + "]";
	
	}

	private void updateBaseProfile(double iteration, Profile profile) {
		
		LinearAssignmentProblem linAssProblem = new LinearAssignmentProblem(omega, p, 0, profile, prediction);
		Profile permutedProfile = linAssProblem.solve();		
		calculateUpdate(iteration, permutedProfile);
	
	}

}